import rospy
import tf2_ros
import roslib
import time
import math
import numpy
import baxter_interface
import requests
import json
import os
import sys
import argparse
import cv2
import cv_bridge
from sensor_msgs.msg import (
    Image,
)



rospy.init_node('info4410')
buf = tf2_ros.Buffer()
listener = tf2_ros.TransformListener(buf)

FRAMES = [
	'head',
	'neck',
	'torso',
	'left_shoulder',
	'left_elbow',
	'left_hand',
	'left_hip',
	'left_knee',
	'left_foot',
	'right_shoulder',
	'right_elbow',
	'right_hand',
	'right_hip',
	'right_knee',
	'right_foot',
] 

def get_frames(i, right_limb, left_limb):
	while not rospy.is_shutdown():
		left_angles = {}
		right_angles = {}
		curr_frames = {}
		for frame in FRAMES:
			num = frame+"_"+str(i)
			lookup_transform = buf.lookup_transform('camera_link', num, rospy.Time(0))
			curr_coords = lookup_transform.transform.translation
			curr_frames[frame] = curr_coords
		left_e1 = e1(curr_frames['left_elbow'], curr_frames['left_hand'], curr_frames['left_shoulder'])
		right_e1 = e1(curr_frames['right_elbow'], curr_frames['right_hand'], curr_frames['right_shoulder'])
		left_s1 = s1(curr_frames['left_shoulder'], curr_frames['left_hip'], curr_frames['left_elbow'])
		right_s1 = s1(curr_frames['right_shoulder'], curr_frames['right_hip'], curr_frames['right_elbow'])
		right_s0, right_e0 = e0(curr_frames['torso'], curr_frames['right_elbow'], curr_frames['right_shoulder'], curr_frames['left_shoulder'], curr_frames['right_hand'])
		left_s0, left_e0 = e0(curr_frames['torso'], curr_frames['left_elbow'], curr_frames['left_shoulder'], curr_frames['right_shoulder'], curr_frames['left_hand'])
				
		#right_angles = {'right_s0':right_s0, 'right_s1':right_s1, 'right_e0': right_e0, 'right_e1': right_e1, 'right_w0':0.0, 'right_w1': 0.0, 'right_w2':0.0}
		right_angles = {'right_s0':right_s0, 'right_s1':right_s1, 'right_e0': right_e0, 'right_e1': right_e1, 'right_w0':0.0, 'right_w1': 0.0, 'right_w2':0.0}
		
#left_angles = {'left_s0':left_s0, 'left_s1':left_s1, 'left_e0': left_e0, 'left_e1': left_e1, 'left_w0':0.0, 'left_w1': 0.0, 'left_w2':0.0}
		left_angles = {'left_s0':left_s0, 'left_s1':left_s1, 'left_e0': left_e0, 'left_e1': left_e1, 'left_w0':0.0, 'left_w1': 0.0, 'left_w2':0.0}

		print ("sending to baxter now...")
	
		#{'left_w0': 0.0, 'left_w1': 0.0, 'left_w2': 0.0, 'left_e0': 1.5489146164938268, 'left_e1': 0.5570162744976841, 'left_s0': -0.1800012495433323, 'left_s1': -0.5210456958143461}
		#time.sleep(1)
		#if(right_s1 < 0 or left_s1 < 0):
		'''print("hand raised")
		data = {'add':'1','name':'Baxter'}
		r = requests.get("http://robot-ramsey.herokuapp.com/api/addBaxter")
		picnum = min(4,r.json()['numPeople'])
		send_to_baxter(right_limb, left_limb)
		send_image('i'+ str(picnum) +'.png')'''
		#else:
			#print("HAND NOT RAISED!!!!!!!!")
		#print right_angles['right_e0']


def get_frame_num(right_limb, left_limb):
	while True:
		for i in range(1,10):
			try:
				lookup_transform = buf.lookup_transform('camera_link', 'head_'+str(i), rospy.Time(0))
				if lookup_transform:
					get_frames(i, right_limb, left_limb)
					return
			except (tf2_ros.LookupException, tf2_ros.ConnectivityException):
				continue


def distance(v1, v2):
	dist_sq = (v2.x-v1.x)**2+(v2.y-v1.y)**2+(v2.z-v1.z)**2
	return dist_sq**(0.5)

def get_vector(v1, v2):
	return [v2.x-v1.x, v2.y-v1.y, v2.z-v1.z]

def dot_product(v1, v2):
	#return magnitude(v1) * magnitude(v2) * math.cos(angle)
	return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2]

def magnitude(v):
	sq = v[0]**2 + v[1]**2 + v[2]**2
	return sq**0.5

def get_angle_from_points(p1,p2,p3):
	v_12 = get_vector(p1, p2)
	v_13 = get_vector(p1, p3)
	return math.acos( dot_product( v_12, v_13 ) / ( magnitude(v_12) * magnitude(v_13) ) )

def e1(elbow, shoulder, hand):
	return -1 * ( get_angle_from_points(elbow, shoulder, hand) - math.pi )

def s1(elbow, shoulder, hand):
	angle = get_angle_from_points(elbow, shoulder, hand) * -1 + math.pi/3

	return angle if angle > -1.27 else -1.27

def s0(torso, elbow, shoulder, other_shoulder):
	v_t1 = get_vector(torso, shoulder)
	v_t2 = get_vector(torso, other_shoulder)
	normal_t = numpy.cross(v_t1, v_t2)

	v_12 = get_vector(shoulder, other_shoulder)
	v_se = get_vector(shoulder, elbow)

	v_d = numpy.cross(normal_t, v_12)
	normal_s = numpy.cross(v_d, v_se)
	
	return normal_t, -1 * math.acos( dot_product( normal_t, normal_s ) / ( magnitude(normal_t) * magnitude(normal_s) ) ) + math.pi/3

def e0(torso, elbow, shoulder, other_shoulder, hand):
	v_eh = get_vector(elbow, hand)
	v_es = get_vector(elbow, shoulder)
	normal_e = numpy.cross(v_eh, v_es)

	normal_t, s_0 = s0(torso, elbow, shoulder, other_shoulder)

	e_0 = (math.acos( dot_product( normal_t, normal_e ) / ( magnitude(normal_t) * magnitude(normal_e) ) ) - math.pi) * -1
	
	return s_0, e_0

def send_to_baxter(right_limb, left_limb):
    right_angles={'right_s0': -0.7405292245056153, 'right_s1': 0.33440781137695313, 'right_w0': 2.628476077038574, 'right_w1': -0.21130585328979493, 'right_w2': 1.0093593572753907, 'right_e0': -3.0418838989013675, 'right_e1': 1.9665633678222658}
    left_angles={'left_w0': 1.523242920629883, 'left_w1': 0.9372622603271485, 'left_w2': 0.3689223790649414, 'left_e0': -1.9167089922729494, 'left_e1': -0.04985437554931641, 'left_s0':1.0266166411193849, 'left_s1': 1.0526943144836427}
	
    left_limb.set_joint_positions(left_angles)
    right_limb.set_joint_positions(right_angles)

#Sending an image to baxter   
def send_image(path):
	print(path)
	img = cv2.imread(path)
	msg = cv_bridge.CvBridge().cv2_to_imgmsg(img, encoding="bgr8")
	pub = rospy.Publisher('/robot/xdisplay', Image, latch=True)
	pub.publish(msg)
	# Sleep to allow for image to be published.	

send_image('default.png')
txt = raw_input('arm up? ')
if len(txt) > 0:
    while True:
        try:
            right_limb = baxter_interface.limb.Limb('right')
            left_limb = baxter_interface.limb.Limb('left')
            if right_limb and left_limb:
                print("hand raised")
                data = {'add':'1','name':'Baxter'}
                r = requests.get("http://robot-ramsey.herokuapp.com/api/addBaxter")
                picnum = min(4,r.json()['numPeople'])
                send_to_baxter(right_limb, left_limb)
                send_image('i'+ str(picnum) +'.png')
                #get_frame_num(right_limb, left_limb)
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException):
            continue



