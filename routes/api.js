var express = require('express');
var router = express.Router();

function getQueue(db, time) {
	db.customers.find({time: {$lt: time}}, function(err, queue) {
		res.send(queue);
	});
}

router.post('/api/getUser', function(req, res, next) {
	var db = req.db;
	var name = req.body.name;

	if (req.body.add == "1") {
		var d = new Date();
		var time = d.getTime();
		db.customers.update({name: name}, {name: name, time: time}, {upsert: true}, function(err, success) {
			db.customers.find({time: {$lt: time}}, function(err, queue) {
				res.send({queue: queue, inLine: true});
			});
		});
	} else {
		db.customers.findOne({name: name}, function(err, customer) {
			if (customer) {
				var time = customer.time;
				db.customers.find({time: {$lt: time}}, function(err, queue) {
					res.send({queue: queue, inLine: true});
				});
			} else {
				var d = new Date();
				var time = d.getTime();
				db.customers.find({time: {$lt: time}}, function(err, queue) {
					res.send({queue: queue, inLine: false});
				});
			}
		});
	}
	
});

router.get('/api/addBaxter', function(req, res, next) {
	var db = req.db;
	var d = new Date();
	var time = d.getTime();

	db.customers.update({name: 'Baxter'}, {name: 'Baxter', time: time}, {upsert: true}, function(err, success) {
		db.customers.find({time: {$lt: time}}, function(err, queue) {
			res.send({numPeople: queue.length, inLine: true});
		});
	});

});

router.get('/api/removeBaxter', function(req, res, next) {
	var db = req.db;
	db.customers.remove({name: 'Baxter'}, function(err, success) {
		res.send(success);
	});
});

router.post('/api/removeUser', function(req, res, next) {
	var db = req.db;
	var name = req.body.name;

	db.customers.remove({name: name}, function(err, success) {
		res.send(success);
	});
});

module.exports = router;
