var ip;

$(document).ready(function() {

	setInterval(function() {updateCustomer(0);}, 2000);

	$.get("http://ipinfo.io", function(response) {
		ip = response.ip;
	}, "jsonp");

	
	$(document).on("click", "#add-customer", function() {
		updateCustomer(1);
	});

	$(document).on("click", "#remove-customer", function() {
		removeCustomer();
	});

});


function updateCustomer(add) {
	$.ajax({
		url: '/api/getUser',
		data: {add: add, name: ip},
		dataType: 'JSON',
		method: 'POST',
		success: function(response) {
			if (response.inLine)
				$("#queue-num").text("You have " + response.queue.length + " people in front of you!");
			else
				$("#queue-num").text("You are not currently signed up. There are " + response.queue.length + " people in line now!");
		}
	});
}

function removeCustomer(add) {
	$.ajax({
		url: '/api/removeUser',
		data: {name: ip},
		dataType: 'JSON',
		method: 'POST',
		success: function(response) {
			$("#queue-num").text("Glad we could help you out!");
		}
	});
}


